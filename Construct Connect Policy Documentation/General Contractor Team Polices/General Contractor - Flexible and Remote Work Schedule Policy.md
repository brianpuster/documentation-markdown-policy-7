title: General Contractor - Flexible and Remote Work Schedule Policy
published: 2019-09-11
author: Jon Kost - EVP and GM General Contractor

## GC SALES, CUSTOMER SUCCESS AND IMPLEMENTATION FLEXIBLE SCHEDULE POLICY
## GC Sales, Customer Success, and Implementation Flexible Schedule Policy

The company is committed to a work environment that accommodates our People First/Family First values. Each team offers a flexible/telecommuting schedule based on business sustainability and individual performance. The following basic requirements must be met for all teams:

- Team members must have a satisfactory attendance and performance record
- The work week for all full-time regular team members is 40 hour per week and 8 hours per day
- Flexible/telecommuting work schedules for non-exempt team members should include an unpaid meal period of 30 minutes at minimum
- There will be times when team members are required to depart from the flexible/telecommuting work schedule to accommodate changing situations and staffing needs. Reasonable notice will be provided when possible
- The day before a holiday falling on a work day is treated as a normal 8 hour business day.

In addition to the basic requirements required for all teams by the company, the **GC Sales, Customer Success, and Implementation Team Policy** is outlined below:

## GC Sales, Customer Success, and Implementation Team Policy
### Early Release Friday

- Team members may leave 1.5 hours earlier than their normal schedule as long as individual performance goals are being met and 6.5 hours are worked for the day.
- Any Friday that falls on the last selling day of the month is excluded from this benefit.
- Team members on a PIP are excluded from this benefit.
- Managers reserve the right to ask team members to stay for their entire schedule due to business needs.
- Early release and PTO cannot be combined. If PTO is taken on a Friday the entire day must be coded as PTO (8 hours). If a partial day is taken, PTO and hours worked must equal 8 hours. Lunch can be taken, but 6.5 hours should be worked that day and if salaried non-exempt, recorded as such in UltiPro.

### Schedule Changes and Telecommuting

- Our team requires that team members who live within 50 miles of an office attend work at that office throughout the week.
- Our team offers the opportunity to work from to meet personal challenges/needs as they arise, however, this does require prior approval from the team members manager.
- In cases where a formal telecommuting arrangement has been found to be the best fit for the business and for the team members, a telecommuting agreement must be completed by the manager and team member.
- Up to two hours may be made up in a week without using PTO.

Team member acknowledges that they have read and understand the terms of
this agreement.
