title: Request to Work Remote From Another State
author: Monica Brooksbank
published: 2019-09-27
effective date: 2019-09-27

------

#### Policy level:

- Important Not Urgent

#### Author:

- Monica Brooksbank

#### Approver(s):

- Senior Finance
- VP People & Culture

#### Location or Team Applicability

- All

------

#### Policy text:

Occasionally a team member will at their own request, want to relocate to a State other than their current ConstructConnect home State location.  

The team member’s request will be reviewed and approved in accordance with the following guidelines:


###### Manager must send their request via e-mail to the Director and VP of their Team, describing the nature of the request, the new location and the business reason supporting the request.
- Requests must be submitted at least 30 days prior to the relocation
- Team member must  be employed with the Company for at least 1 year
- Team member must not be on a Performance Improvement Plan anytime during the previous 12 months
- Team member will not be reimbursed by the Company for their moving expenses

###### If approved by VP of the Team, the VP will submit the team member’s request to the People & Culture Team to assist in the completion of the following:
- People & Culture – verifies team member is not on a Performance Improvement Plan and has been employed with us for more than 1 year.
- IT Team – verifies we have the equipment needed to provide to team member for the remote move.  **IT team will document any needed equipment and costs associated.  A requisition will then need to be submitted to obtain a PO for purchase of any needed equipment that was not already part of any previous forecast.  A change or upgrade of equipment will need to be justified, if the upgrade is dictated because of the move.**
- Finance / Payroll Team – verifies we have State withholding and State unemployment account established in the State the team member is relocating to. If we are not set up in the State, finance approval will be required for the opening of the new employer account in that State.

**At the conclusion of these reviews, a People & Culture representative will circulate the request for the remaining approvals (see below).**

------

##### To be completed by the Manager:

Team member name: 
Date of hire:
Team: 
Team manager:

##### Detail the business reason for this request:

------

------

------

------

------

V.P. of Team:
Approve or Denied

People & Culture CPO:
Approve or Denied

IT Team:
Approve or Denied 

Senior Finance:
Approve or Denied 

Payroll Team:
State Withholding & Unemployment account opened               Yes   or   No 