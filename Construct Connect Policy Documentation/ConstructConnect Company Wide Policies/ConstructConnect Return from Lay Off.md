title: Return from Lay Off
author: Julie Storm - Chief People Officer
published: 2020-05-21

#### Policy level: 
- Very Important 

#### Author: 
- Julie Storm - Chief People Officer 

#### Approver(s):
- Julie Storm - Chief People Officer
- Dave Conway - CEO 

#### What location(s) and/or team(s) does the policy apply to?
- All locations, all teams 

#### Effective date: 
- 2020-05-18 

---

#### Policy text: 
**Policy**

Laid off team members may apply for approved open positions at ConstructConnect following the exhaustion of their severance weeks. Example: A team member who received 12 weeks of severance can apply for open positions in the 13th week.

If the team member was laid off in good standing (not on a PIP) they should be considered for rehire. This includes those that may not have been at the company long enough to adequately measure performance metrics.

Should a team member who was laid off apply for an open position for which they are qualified, their application will be reviewed for a potential interview. they will be considered along with all qualified applicants, both internal and external. The most qualified applicants will then be sent to the hiring manager for review. The most qualified individuals will be selected for interview and potential hire.

**Return Process**

If selected for the position, seniority with the company will be restored if the team member has been gone less than one year. This will include returning with the original date of hire and receiving the last PTO accrual rate based on years of service. Vesting in 401k will also be based on the original hire date.

Those who have been gone one year or more will be treated as a new hire.

